const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const rename = require('gulp-rename');

let tasks = [
    { type: 'sass', watch: ['scss/theme/*.scss', 'scss/theme/**/*.scss'], source: 'scss/theme/base.scss', target: 'base.css' },
    { type: 'sass', watch: ['scss/bootstrap/*.scss', 'scss/bootstrap/**/*.scss'], source: 'scss/bootstrap/bootstrap.scss', target: 'bootstrap.css' },
];

let taskTypes = {
    sass: task => () => gulp.src(task.source)
        .pipe(sourcemaps.init())
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(rename(task.target))
        .pipe(autoprefixer())
        .pipe(sourcemaps.write(''))
        .pipe(gulp.dest('css')),
};

tasks.forEach(task => {
    if (taskTypes[task.type]) {
        task.exec = taskTypes[task.type](task);
    } else {
        throw new Error(`No task type for ${task.type} defined!`);
    }
    return gulp.task(task.source, task.exec);
});

gulp.task('make', gulp.parallel(...tasks.map(task => task.source)));
gulp.task('watch-for-changes', () => {
    tasks.map(task => {
        gulp.watch(task.watch, gulp.series(task.source));
    });
});
gulp.task('watch', gulp.series('make', 'watch-for-changes'));

