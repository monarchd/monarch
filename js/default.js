/**
 * @file
 * Theme library.
 */


(function ($) {
  window.addEventListener('load', function () {
    var classPair = {
      '.tabledrag-toggle-weight': ['btn', 'btn-outline-secondary', 'mb-3'],
    };
  
    for (var className in classPair) {
      var obj = $(className);
      obj.addClass(classPair[className].join(' '));
    }  
  });
})(jQuery);

