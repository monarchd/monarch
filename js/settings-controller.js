(function ($, Drupal) {
  Drupal.behaviors.MonarchSettings = {
    attach: function (context, settings) {
      function showColorRows(visible) {
        if (visible) {
          $('#system-theme-settings .color-row').removeClass('d-none');
        } else {
          $('#system-theme-settings .color-row').addClass('d-none');
        }
      }

      $('#system-theme-settings', context).once('attach').each(function () {
        $('#edit-color-scheme').change(function () {
          showColorRows(this.value === 'custom');
        });

        $('#system-theme-settings .d-none').each(function () {
          $(this).removeClass('d-none');
        });

        showColorRows($('#edit-color-scheme').val() === 'custom');
      });
    }
  };
})(jQuery, Drupal);